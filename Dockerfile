FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build-env


COPY ./ /app
WORKDIR app/src
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "AS.Redis.Connector.dll"]
